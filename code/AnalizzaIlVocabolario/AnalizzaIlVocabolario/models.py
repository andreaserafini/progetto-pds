from django.contrib.auth.models import AbstractUser
from django.db import models


# Utenti.objects.create_user("andreas","andreaserafinis@gmail.com","Password123",
#                           first_name="Andrea", last_name="Serafini", bio="IT student in Modena",
#                           l_min=0, blacklist="")

class Utenti(AbstractUser):
    bio = models.TextField()
    blacklist = models.TextField()
    l_min = models.PositiveIntegerField()

    class Meta:
        db_table = "utenti"


class Testi(models.Model):
    titolo = models.CharField(max_length=100, primary_key=True)
    autore = models.ForeignKey(Utenti, on_delete=models.CASCADE)
    indice_legg = models.PositiveIntegerField()
    data = models.DateField(auto_now_add=True)
    ora = models.TimeField(auto_now_add=True)

    class Meta:
        db_table = "testi"


class Parole(models.Model):
    parola = models.CharField(max_length=20, primary_key=True)

    class Meta:
        db_table = "parole"


class Contiene(models.Model):
    class Meta:
        unique_together = (('titolo', 'parola'),)
        db_table = "contiene"

    titolo = models.ForeignKey(Testi, on_delete=models.CASCADE)
    parola = models.ForeignKey(Parole, on_delete=models.DO_NOTHING)
    freq = models.PositiveIntegerField()

