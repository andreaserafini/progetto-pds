from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.sites.shortcuts import get_current_site
from django.db import IntegrityError
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.core.mail import EmailMessage
import Backend.TextAnalizer
from AnalizzaIlVocabolario.models import Utenti
from AnalizzaIlVocabolario.token_generator import account_activation_token
from Backend.TextAnalizer import apply_blacklist_filter, apply_minchar_filter
from AnalizzaIlVocabolario.forms import RegistrationForm
import Backend.ricerca
from Backend.dbsave import salva_blacklist, salva_testo, salva_l_min
from Backend.confronta import unisci_testi, indici_gulpease
from Backend.dbload import carica_blacklist, carica_diz_freq, carica_l_min, carica_gulpease, carica_legacy
import Backend.ordinamento
from googletrans import LANGCODES, Translator


def Home(request):
    return render(request, 'home.html')


def CaricaParole(request):
    if request.POST:
        if 'carica' in request.POST:
            titolo = request.POST["carica"]
            text, autore = carica_diz_freq(titolo)
            request.session['titolo'] = titolo
            request.session['words_with_frequency'] = text
            request.session['autore'] = autore
            request.session['gulpease'] = carica_gulpease(titolo)
            try:
                blacklist = carica_blacklist(request.user)
                l_min = carica_l_min(request.user)
                parole_filtrate = apply_blacklist_filter(text, blacklist)
                parole_filtrate = apply_minchar_filter(parole_filtrate, l_min)
            except Exception:
                parole_filtrate = text

                # parole_filtrate = OrderedDict(sorted(parole_filtrate.items(), key=lambda x: x[1], reverse=True))
            return render(request, 'visualizza_carica.html',
                          {'titolo': titolo, 'gulpease': carica_gulpease(titolo), 'frequenza': parole_filtrate,
                           'autore': autore})

        if 'ordine' in request.POST:
            ordine = request.POST["ordine"]
            titolo = request.session['titolo']
            words_with_frequency = request.session['words_with_frequency']
            autore = request.session['autore']
            try:
                blacklist = carica_blacklist(request.user)
                l_min = carica_l_min(request.user)
                parole_filtrate = apply_blacklist_filter(words_with_frequency, blacklist)
                parole_filtrate = apply_minchar_filter(parole_filtrate, l_min)
            except Exception:
                parole_filtrate = words_with_frequency
            try:
                gulpease = request.session["gulpease"]
            except:
                gulpease = carica_gulpease(titolo)

            parole_filtrate = Backend.ordinamento.ordina(parole_filtrate, ordine)
            print(parole_filtrate)
            return render(request, 'visualizza_carica.html',
                          {'titolo': titolo, 'gulpease': gulpease, 'frequenza': parole_filtrate,
                           'autore': autore})
        return render(request, 'visualizza_carica.html')


def VisualizzaParole(request):
    if request.POST:
        if 'elabora' in request.POST:
            titolo = request.POST["titolo"]
            testo = request.POST["testo"]
            text = Backend.TextAnalizer.Text(titolo, testo)
            try:
                blacklist = carica_blacklist(request.user)
                l_min = carica_l_min(request.user)
                parole_filtrate = apply_blacklist_filter(text.words_with_frequency, blacklist)
                parole_filtrate = apply_minchar_filter(parole_filtrate, l_min)
            except Exception:
                parole_filtrate = text.words_with_frequency
            request.session['titolo'] = text.title
            request.session['gulpease'] = text.gulpease_index
            request.session['words_with_frequency'] = text.words_with_frequency
            # parole_filtrate = OrderedDict(sorted(parole_filtrate.items(),  key=lambda x: x[1], reverse=True))
            try:
                autore = request.user
            except Exception:
                pass
            return render(request, 'visualizza_parole.html',
                          {'titolo': text.title, 'gulpease': text.gulpease_index, 'frequenza': parole_filtrate,
                           'autore': autore})
        if 'salva' in request.POST:
            titolo = request.session['titolo']
            gulpease = request.session['gulpease']
            words_with_frequency = request.session['words_with_frequency']
            author = request.user
            try:
                blacklist = carica_blacklist(request.user)
                l_min = carica_l_min(request.user)
                parole_filtrate = apply_blacklist_filter(words_with_frequency, blacklist)
                parole_filtrate = apply_minchar_filter(parole_filtrate, l_min)
            except Exception:
                parole_filtrate = words_with_frequency

            # salavataggio db
            try:
                Backend.dbsave.salva_testo(titolo, gulpease, words_with_frequency, author)
            except IntegrityError as e:
                error = "Un testo con questo titolo esiste già!"
                return render(request, 'visualizza_parole.html',
                              {'titolo': titolo, 'gulpease': gulpease,
                               'frequenza': parole_filtrate, 'autore': author, 'errore': error})

            # parole_filtrate = OrderedDict(sorted(parole_filtrate.items(), key=lambda x: x[1], reverse=True))
            # ristampa pagina
            successo = "Testo salvato correttamente!"
            return render(request, 'visualizza_parole.html',
                          {'titolo': titolo, 'gulpease': gulpease,
                           'frequenza': parole_filtrate, 'successo': successo})
        if 'ordine' in request.POST:
            ordine = request.POST["ordine"]
            titolo = request.session['titolo']
            words_with_frequency = request.session['words_with_frequency']

            try:
                blacklist = carica_blacklist(request.user)
                l_min = carica_l_min(request.user)
                parole_filtrate = apply_blacklist_filter(words_with_frequency, blacklist)
                parole_filtrate = apply_minchar_filter(parole_filtrate, l_min)
            except Exception:
                parole_filtrate = words_with_frequency
            try:
                gulpease = request.session["gulpease"]
            except:
                gulpease = carica_gulpease(titolo)

            parole_filtrate = Backend.ordinamento.ordina(parole_filtrate, ordine)
            return render(request, 'visualizza_parole.html',
                          {'titolo': titolo, 'gulpease': gulpease, 'frequenza': parole_filtrate})

    return render(request, 'visualizza_parole.html')


def InserisciTesto(request):
    return render(request, 'inserisci_testo.html')


@login_required(login_url='login')
def ricercaTesti(request):
    if request.POST:
        if 'barraRicerca' in request.POST:
            ricerca = request.POST["barraRicerca"]
            # Chiaro eh?
            request.session['titoliCercati'] = Backend.ricerca.cerca(ricerca)
            return render(request, 'ricerca_testi.html', {'titoliCercati': request.session['titoliCercati'],
                                                          'listaConfronto': request.session['listaConfronto']})
        if 'aggiungi' in request.POST:
            listaC = request.session['listaConfronto']
            listaC.append(request.POST['aggiungi'])
            request.session['listaConfronto'] = listaC
            return render(request, 'ricerca_testi.html', {'titoliCercati': request.session['titoliCercati'],
                                                          'listaConfronto': request.session['listaConfronto']})
        if 'rimuovi' in request.POST:
            listaC = request.session['listaConfronto']
            listaC.remove(request.POST['rimuovi'])
            request.session['listaConfronto'] = listaC
            return render(request, 'ricerca_testi.html', {'titoliCercati': request.session['titoliCercati'],
                                                          'listaConfronto': request.session['listaConfronto']})
    else:
        request.session['listaConfronto'] = []
    return render(request, 'ricerca_testi.html')


@login_required(login_url='login')
def Blacklist(request):
    utente = request.user
    l_min = carica_l_min(utente)
    if request.POST:
        if 'rimuovi' in request.POST:
            blackword = request.POST["rimuovi"]
            blacklist_temp = carica_blacklist(utente)
            blacklist_temp.remove(blackword)
            salva_blacklist(utente, blacklist_temp)
        elif 'inserisci' in request.POST:
            blackword = request.POST["blackword"]
            blacklist_temp = carica_blacklist(utente)
            if blackword not in blacklist_temp:
                # query per l'inserimento dei dati nel db
                blacklist_temp.append(blackword)
                salva_blacklist(utente, blacklist_temp)
        if 'aggiorna' in request.POST:
            l_min = request.POST['lunghezzaminima']
            salva_l_min(utente, l_min)
    # query per prendere blacklist utente
    return render(request, 'blacklist.html', {'blacklist': carica_blacklist(utente), 'lunghezza_minima': l_min})


@login_required(login_url='login')
def Confronto(request):
    titoli = request.session["listaConfronto"]
    tabella = unisci_testi(titoli)
    gulpease_list = indici_gulpease(titoli)

    return render(request, 'confronto_testi.html',
                  {'frequenza': tabella, 'titoli': titoli, 'gulpease': gulpease_list})


def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            email_subject = 'Analizza Il Vocabolario : Conferma Account'
            message = render_to_string('registration/activate_account.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            to_email = form.cleaned_data.get('email')
            email = EmailMessage(email_subject, message, to=[to_email])
            email.send()
            return render(request,'home.html')
            #return HttpResponseRedirect(reverse_lazy('login'))
            #return HttpResponseRedirect(reverse_lazy('home'))
        else:
            return render(request, 'register.html', {'form': form})
    else:
        form = RegistrationForm()
        args = {'form': form}
        return render(request, 'register.html', args)


def activate_account(request, uidb64, token):
    try:
        uid = force_bytes(urlsafe_base64_decode(uidb64))
        user = Utenti.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, Utenti.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        return HttpResponse('Il tuo account è stato attivato correttamente')
    else:
        return HttpResponse('Il tuo account è già attivo!')


def grafico(request):
    titolo = request.session["titolo"]
    words = request.session["words_with_frequency"]
    print(titolo)
    try:
        blacklist = carica_blacklist(request.user)
        l_min = carica_l_min(request.user)
        parole_filtrate = apply_blacklist_filter(words, blacklist)
        parole_filtrate = apply_minchar_filter(parole_filtrate, l_min)
    except Exception:
        parole_filtrate = words

    labelsmag = []
    datamag = []
    print(len(parole_filtrate) / 100)
    for key, value in parole_filtrate.items():
        if value > len(parole_filtrate) / 100:
            labelsmag.append(key)
            datamag.append(value)

    return render(request, 'grafico.html', {
        'titolo': titolo,
        'labelsmag': labelsmag,
        'datamag': datamag,

    })


def legacy(request):
    frequenza = carica_legacy()
    l_min = carica_l_min(request.user)
    blacklist = carica_blacklist(request.user)

    parole_filtrate = apply_blacklist_filter(frequenza, blacklist)
    parole_filtrate = apply_minchar_filter(parole_filtrate, l_min)

    labels = []
    data = []
    for key, value in parole_filtrate.items():
        if value > len(parole_filtrate) / 100:
            labels.append(key)
            data.append(value)

    if request.POST:
        ordine = request.POST["ordine"]
        parole_filtrate = Backend.ordinamento.ordina(parole_filtrate, ordine)

    return render(request, 'legacy.html', {'frequenza': parole_filtrate, 'labels': labels, 'data': data})

def traduci(request):
    traduttore = Translator()
    if request.POST:
        if 'traduci'in request.POST:
            lingua = request.POST['lingua']
            parola = request.session['parola']
            parola_tradotta = traduttore.translate(parola,LANGCODES[lingua],src='it').text
            lingua = traduttore.translate(lingua,dest='it',src=LANGCODES['english']).text

        if 'traduci_init' in request.POST:
            parola = request.POST['traduci_init']
            lingua = 'inglese'
            request.session['parola'] = parola
            parola_tradotta = traduttore.translate(parola,dest='en',src='it').text

        return render(request,'traduci.html',{'lingue' : LANGCODES, 'parolaIT': parola, 'parolaTR':parola_tradotta,'linguaHeader' : lingua})

    return render(request,'traduci.html',{'lingue': LANGCODES})