import TextAnalizer

def test_remove_all_not_letters():
    
    example = "accadrà domani. && ciaò $à"
    example_succes = "accadrà domani     ciaò  à"
    
    assert TextAnalizer.remove_all_not_letters(example) == example_succes

def test_remove_stopwords():
    
    example = "Io mangio la mela"
    example_success = ["mangio", "mela"]

    assert TextAnalizer.remove_stopwords(example) == example_success

def test_remove_puntuation():

    example = "Ciao, come stai? Tutto bene."
    example_success = "Ciao  come stai  Tutto bene "

    assert TextAnalizer.remove_puntuation(example) == example_success

def test_remove_numbers():

    example = "C0ngr4ts h4ck3r!"
    example_success = "C ngr ts h ck r!"

    assert TextAnalizer.remove_numbers(example) == example_success

def test_count_words_frequency():

    example = ["è", "una", "lista", "di", "è", "una",  "di", "questa", "è", "una", "lista", "di", "questa", "è", "una", "lista", "di", "parole", "una"]
    example_success = {'è' : 4,'una' : 5, 'lista' : 3, 'di' : 4,'questa' : 2, 'parole' : 1}

    assert TextAnalizer.count_words_frequency(example) == example_success

def test_count_letters_in_word_list():

    example = ["gatto", "cane", "maiale", "pulcino"]
    example_success = 22

    assert TextAnalizer.count_letters_in_word_list(example) == example_success

def test_count_phrases_in_text():

    example = "Sempre caro mi fu quest'ermo colle,\
E questa siepe, che da tanta parte\
Dell'ultimo orizzonte il guardo esclude.\
 Ma sedendo e mirando, interminati\
Spazi di là da quella, e sovrumani\
Silenzi, e profondissima quiete\
Io nel pensier mi fingo; ove per poco\
Il cor non si spaura. E come il vento\
Odo stormir tra queste piante, io quello\
Infinito silenzio a questa voce\
Vo comparando: e mi sovvien l'eterno,\
E le morte stagioni, e la presente\
E viva, e il suon di lei. Così tra questa\
Immensità s'annega il pensier mio:\
E il naufragar m'è dolce in questo mare."

    example_success = 4

    assert TextAnalizer.count_phrases_in_text(example) == example_success

def test_gulpease():

    # Numero di lettere -> 128
    # Numero di parole -> 30
    # Numero di frasi -> 4
    # (Contare per credere!)
    # Expected gulpease = 89 + ((300*4 - 10*128) / 30) -> 86
    example = "Tanto va la gatta al lardo, che ci lascia lo zampino. Rosso di sera, bel tempo si spera.\
                Una rondine non fa primavera. Al baciarsi presto tien dietro il coricarsi."


    text = TextAnalizer.Text("","",example)
    assert text.gulpease_index == 86