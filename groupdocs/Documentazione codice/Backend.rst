Backend package
===============

Submodules
----------

Backend.TextAnalizer module
---------------------------

.. automodule:: Backend.TextAnalizer
   :members:
   :undoc-members:
   :show-inheritance:

Backend.confronta module
------------------------

.. automodule:: Backend.confronta
   :members:
   :undoc-members:
   :show-inheritance:

Backend.dbload module
---------------------

.. automodule:: Backend.dbload
   :members:
   :undoc-members:
   :show-inheritance:

Backend.dbsave module
---------------------

.. automodule:: Backend.dbsave
   :members:
   :undoc-members:
   :show-inheritance:

Backend.ordinamento module
--------------------------

.. automodule:: Backend.ordinamento
   :members:
   :undoc-members:
   :show-inheritance:

Backend.ricerca module
----------------------

.. automodule:: Backend.ricerca
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: Backend
   :members:
   :undoc-members:
   :show-inheritance:
